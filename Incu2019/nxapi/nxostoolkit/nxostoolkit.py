import requests
import json

class nexus:
    
    version = "Version goes here"
    platform = "platform goes here"
    
    #Location and port for the Nexus switch. Needs to be changed when using another device.
    ##Here using the always on Nexus switch online.
    online_nexus = {"ip": "sbx-nxos-mgmt.cisco.com",
                       "port": "80",
                   }
    Apic_cookie = {}
    
    def __init__(self):
        self.version = "Version has not been set, authenticate first"
        self.platform = "Platform has not been set, authenticate first"
        
        
    def getCookie(self, resp):
        #While executing other commands, they will return an error code of 403, pointing at invalid/ missing cookie
        ##This code creates the cookie when the user authenticates to the switch
        
        auth = json.loads(resp.text)
        
        #Path to the cookie:
        login_attributes = auth['imdata'][0]['aaaLogin']['attributes']
        auth_token = login_attributes['token']
        
        #Adapting the value of the cookie every time the user logs in, so it can be used for future functions.
        self.Apic_cookie = {}
        self.Apic_cookie['APIC-Cookie'] = auth_token
        
    def getVersion(self):
    
        jsonrpc_headers = {'Content-Type': 'application/json-rpc'}
        uri = 'http://{}/api/mo/sys/showversion.json'.format(self.online_nexus['ip'])
        response = requests.get(uri,
                                headers = jsonrpc_headers,
                                cookies = self.Apic_cookie
                               )
        data = response.json()
        self.version = data['imdata'][0]['sysmgrShowVersion']['attributes']['nxosVersion']
         
        
        
    def authenticate(self, user, passw):
        
        
        ####################################################################
        #                                                                  #
        # This function will log the user to the switch for administration #
        #                                                                  #
        ####################################################################
        
        
        jsonrpc_headers = {'Content-Type': 'application/json-rpc'}
        
        #Passing the "user" and "passw" arguments to login to the switch, the request will be executed with the following url:
        uri = 'http://{}/api/aaaLogin.json'.format(self.online_nexus['ip'])
        payload = {
          "aaaUser": {
            "attributes": {
              "name": user,
              "pwd": passw
            }
          }
        }
        
        response = requests.post(uri, 
                         data=json.dumps(payload),
                         headers=jsonrpc_headers
                        )

        if response.status_code == 200:
            print("Connection successful")
            self.getCookie(response)
            self.getVersion()
        else:
            print("Error, recheck credentials")
            
    def get_interface_status(self, if_name):
        
        
        ########################################################
        #                                                      #
        # This function will return the status of an interface #
        #                                                      #
        ########################################################
        
        
        jsonrpc_headers = {'Content-Type': 'application/json-rpc'}
        uri = 'http://{}/api/mo/sys/intf/phys-[{}].json'.format(self.online_nexus['ip'], if_name)
        
        response = requests.get(uri,
                                headers = jsonrpc_headers,
                                cookies = self.Apic_cookie
                               )
        
        
        #Should be optimized using try, different codes for different messages, ... here simple check if all is good.
        if response.status_code == 200:
            data = response.json()
            ##Here I'm getting a KeyError and also status code of 403(which is = forbidden)?
            ##Have found a solution while transcripting the response to text concerning APIC-cookie
            ##Apparently have to create and pass the cookies for every request when using this method
            intf_status = data['imdata'][0]['l1PhysIf']['attributes']['adminSt']
            
            print('The state of interface {} is {}'.format(if_name, intf_status))
        else:
            print('Error, please re-use authenticate or check if interface name is correct')
        
            
    def configure_interface_desc(self, if_name, description):
        
        
        #############################################################
        #                                                           #
        # This function will modify the description of an interface #
        #                                                           #
        #############################################################
        
        
        jsonrpc_headers = {'Content-Type': 'application/json-rpc'}
        uri = 'http://{}/api/mo/sys/intf/phys-[{}].json'.format(self.online_nexus['ip'], if_name)
        
        payload = {
            "l1PhysIf": {
                "attributes": {
                    "descr": description
                }
            }
        }
        
        response = requests.post(uri, 
                         data=json.dumps(payload),
                         headers=jsonrpc_headers,
                         cookies = self.Apic_cookie
                        )
        
        #Should be optimized using try, different codes for different messages, ... here simple check if all is good.
        if response.status_code == 200:
            print('the description "{}" has successfully been applied to interface {}. Use get_interface_description(if_name) to verify.'.format(description, if_name))
        else:
            print('Error, please re-use authenticate or check if interface-name is correct')
            
        
    def get_interface_description(self, if_name):
        
        
        #############################################################
        #                                                           #
        # This function will return the description of an interface #
        #                                                           #
        #############################################################
        
        
        jsonrpc_headers = {'Content-Type': 'application/json-rpc'}
        uri = 'http://{}/api/mo/sys/intf/phys-[{}].json'.format(self.online_nexus['ip'], if_name)
        
        response = requests.get(uri,
                                headers = jsonrpc_headers,
                                cookies = self.Apic_cookie
                               )
        
        
        #Should be optimized using try, different codes for different messages, ... here simple check if all is good.
        if response.status_code == 200:
            data = response.json()
            intf_descr = data['imdata'][0]['l1PhysIf']['attributes']['descr']
            
            print('The description of interface {} is currently "{}"'.format(if_name, intf_descr))
        else:
            print('Error, please re-use authenticate or check if interface name is correct')